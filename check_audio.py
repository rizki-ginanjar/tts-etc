from os import listdir
from os.path import isfile, join
import sox
from tqdm import tqdm
import numpy as np

wav_root = "/srv/data1/speech/tts/trisa/v2/waves_16k/"
onlyfiles = [f for f in listdir(wav_root) if isfile(join(wav_root, f))]

num_file = len(onlyfiles)
duration_mat = np.zeros(num_file)

for i in tqdm(range(num_file)):
    wav_duration = sox.file_info.duration(wav_root + onlyfiles[i])
    duration_mat[i] = wav_duration

print("Max Duration = %.4f", max(duration_mat))